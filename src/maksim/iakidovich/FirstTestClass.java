package maksim.iakidovich;

public class FirstTestClass {

    private String name;

    public FirstTestClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
