package logan;

public interface IExtensionManager {

    boolean isValid(String fileName);
}
