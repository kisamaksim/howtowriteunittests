package logan;

public class FileExtensionManager implements IExtensionManager {

    public boolean isValid(String fileName) {

        if (fileName.isEmpty()) {
            throw new IllegalArgumentException("file name should be exist");
        }
        if (!fileName.toLowerCase().endsWith(".slf")) {
            return false;
        }
        return true;
    }
}
