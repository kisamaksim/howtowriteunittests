package logan.fakes;

import logan.IWebService;

public class FakeWebService implements IWebService {

    public String lastError;

    @Override
    public void logError(String message) {
        lastError = message;
    }
}
