package logan.fakes;

import logan.IExtensionManager;

public class AlwaysValidFakeExtensionManager implements IExtensionManager {
    public boolean isValid = false;
    @Override
    public boolean isValid(String fileName) {
        return isValid;
    }
}
