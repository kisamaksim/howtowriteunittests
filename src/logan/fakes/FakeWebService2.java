package logan.fakes;

import logan.IWebService;

public class FakeWebService2 implements IWebService {
    public Exception ex;
    @Override
    public void logError(String message) throws Exception {
        if (ex != null) {
            throw ex;
        }
    }

}
