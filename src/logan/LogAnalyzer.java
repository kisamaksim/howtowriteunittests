package logan;

import org.junit.jupiter.params.aggregator.ArgumentAccessException;

import java.io.File;

public class LogAnalyzer {

    public boolean wasLastFileNameValid = true;
    private IExtensionManager manager;
    private IWebService service;
    private IEmailService emailService;

    public void setEmailService(IEmailService emailService) {
        this.emailService = emailService;
    }

    public void setService(IWebService service) {
        this.service = service;
    }

    public LogAnalyzer() {
        manager = new FileExtensionManager();
    }

    public LogAnalyzer(IExtensionManager manager) {
        this.manager = manager;
    }

    public  LogAnalyzer(IWebService webService, IEmailService emailService) {
        this();
        this.service = webService;
        this.emailService = emailService;
    }

    public void analyze(String filename) {
        try {
            if (filename.length() < 8) {
                service.logError("To short filename " + filename);
            }
        } catch (Exception e) {
            emailService.sendEmail("someone@g.c","can't log", e.getMessage());
        }
    }

    public boolean isValidLogFileName(String fileName) {

        wasLastFileNameValid = false;
        boolean isValid = manager.isValid(fileName);
        if (isValid) {
            wasLastFileNameValid = true;
            return true;
        } else {
            return false;
        }
    }


}
