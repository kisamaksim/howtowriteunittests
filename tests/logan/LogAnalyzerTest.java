package logan;

import logan.fakes.AlwaysValidFakeExtensionManager;
import logan.fakes.FakeEmailService;
import logan.fakes.FakeWebService2;
import logan.fakes.FakeWebService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class LogAnalyzerTest {

    private LogAnalyzer analyzer = null;

    @BeforeEach
    void setUp() {
        analyzer = new LogAnalyzer();
    }

    @Test
    void analyze_TooShortFileName_CallsWebService() {
        FakeWebService fakeWebService = new FakeWebService();
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        logAnalyzer.setService(fakeWebService);

        String tooShortFileName = "abc.slf";
        logAnalyzer.analyze(tooShortFileName);

        Assertions.assertEquals("To short filename abc.slf", fakeWebService.lastError);
    }

    @Test
    void Analyze_WebServiceThrowException_SendEmail() {
        FakeEmailService fakeEmailService = new FakeEmailService();
        IWebService stubService = message -> {throw new Exception("fake ex");};
        LogAnalyzer logAnalyzer = new LogAnalyzer(stubService, fakeEmailService);

        logAnalyzer.analyze("abc.txt");

        Assertions.assertEquals("someone@g.c", fakeEmailService.to);
        Assertions.assertEquals("can't log", fakeEmailService.subject);
        Assertions.assertEquals("fake ex", fakeEmailService.body);
    }

    @Tag("fastTest")
    @Test
    void isValidLogFileName_BadExtension_ReturnsFalse() {

        AlwaysValidFakeExtensionManager fakeExtensionManager = new AlwaysValidFakeExtensionManager();
//        fakeExtensionManager.isValid = true;
        LogAnalyzer analyzer = new LogAnalyzer(fakeExtensionManager);

        boolean result = analyzer.isValidLogFileName("filenameblabla.txt");

        Assertions.assertFalse(result);
    }

    @Tag("longTest")
    @ParameterizedTest
    @ValueSource(strings = {"blabla.slf", "blabla.SLF"})
    void isValidLogFileName_ValidExtensions_ReturnTrue(String fileName) {

        boolean result = analyzer.isValidLogFileName(fileName);

        Assertions.assertTrue(result);
    }

    @Test
    void isValidLogFileName_EmptyFileName_ThrowException() {

        LogAnalyzer analyzer = new LogAnalyzer();

        IllegalArgumentException ex = Assertions.assertThrows(IllegalArgumentException.class,
                                                              () -> analyzer.isValidLogFileName(""));
    }

    @Tag("wasLastFileNameValid")
    @ParameterizedTest
    @ValueSource(strings = {"test.slf", "test.FOO"})
    void isValidLogFileName_WhenCalled_ChangesWasLastFileNameValid(String filename) {

        LogAnalyzer analyzer = new LogAnalyzer();

        boolean validLogFileName = analyzer.isValidLogFileName(filename);

        Assertions.assertEquals(validLogFileName, analyzer.wasLastFileNameValid);
    }

    @Tag("wasLastFileNameValid")
    @Test
    void isValidFileName_WhenCalledMethodReturnFalse_ChangesWasLastFileNameValidFalse() {

        LogAnalyzer analyzer = new LogAnalyzer();

        analyzer.isValidLogFileName("file.tmp");

        Assertions.assertFalse(analyzer.wasLastFileNameValid);
    }

    @Tag("wasLastFileNameValid")
    @Test
    void isValidFileName_WhenCalledMethodReturnTrue_ChangesWasLastFileNameValidTrue() {

        LogAnalyzer analyzer = new LogAnalyzer();

        analyzer.isValidLogFileName("file.slf");

        Assertions.assertTrue(analyzer.wasLastFileNameValid);
    }

    @AfterEach
    void tearDown() {

    }
}
